# data-feed

Data feed app

# Prerequisites
1. Download / clone https://github.com/confluentinc/cp-all-in-one
2. cd cp-all-in-one
3. git checkout 5.4.1-post
4. cd cp-all-in-one
5. docker-compose up -d

This will boot-up Kafka server, Schema registry server, Zookeeper

Ref: https://docs.confluent.io/current/quickstart/ce-docker-quickstart.html

# Run Application
1. sh runner.sh

# Access API
http://localhost:8084/api/stocks/sbin
http://localhost:8084/api/stocks/tcs
http://localhost:8084/api/stocks/infy
http://localhost:8084/api/stocks/ioc
http://localhost:8084/api/stocks/hindalco