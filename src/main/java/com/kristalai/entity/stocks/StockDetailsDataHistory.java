package com.kristalai.entity.stocks;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StockDetailsDataHistory {

  private int min;

  private float sum;

  private String timeRange;

}
