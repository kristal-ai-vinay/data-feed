package com.kristalai.entity.stocks;

import java.util.List;

import com.kristalai.entity.common.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document("stock-details")
@AllArgsConstructor
@NoArgsConstructor
public class StockDetailsSummary extends AuditableEntity {

  private String scrip;

  private float total;

  private List<StockDetailsDataHistory> histories;

}
