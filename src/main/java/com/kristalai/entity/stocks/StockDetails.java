package com.kristalai.entity.stocks;

import java.time.Instant;

import com.kristalai.entity.common.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document("stock-details")
@AllArgsConstructor
@NoArgsConstructor
public class StockDetails extends AuditableEntity {

  private String scrip;

  private float ltp;

  private Instant timeStamp;

}
