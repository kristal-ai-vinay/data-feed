package com.kristalai.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Status {

  SUCCESS(200),
  FAILURE(400),
  NOT_FOUND(404),
  UNAUTHORIZED(401),
  INTERNAL_SERVER_ERROR(500);

  private int code;

}
