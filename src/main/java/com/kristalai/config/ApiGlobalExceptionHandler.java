package com.kristalai.config;

import java.util.List;

import com.kristalai.commons.KristalAiResponse;
import com.kristalai.entity.enums.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ApiGlobalExceptionHandler {

  @ExceptionHandler(Exception.class)
  public KristalAiResponse handleException(Exception ex) {
    log.error(
        "************** Global level un handled exception occurred : " + ex.getMessage() + ", " + ex
            .getCause());
    return KristalAiResponse.builder().status(Status.INTERNAL_SERVER_ERROR)
        .statusCode(Status.INTERNAL_SERVER_ERROR.getCode()).data(ex)
        .errors(List.of(ex.getMessage())).build();
  }
}
