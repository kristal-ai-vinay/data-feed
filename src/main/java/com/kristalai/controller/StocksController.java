package com.kristalai.controller;

import java.util.List;

import com.kristalai.commons.KristalAiResponse;
import com.kristalai.commons.exception.KristalAiException;
import com.kristalai.dto.StockDetailsSummaryDTO;
import com.kristalai.entity.enums.Status;
import com.kristalai.entity.stocks.StockDetailsSummary;
import com.kristalai.helpers.KristalAiHelper;
import com.kristalai.service.StocksService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/stocks")
@AllArgsConstructor
@Slf4j
public class StocksController {

  private StocksService stocksService;

  private ModelMapper mapper;

  @GetMapping("/{scrip}")
  public KristalAiResponse getSummary(@PathVariable String scrip) {
    StockDetailsSummaryDTO detailsSummaryDTO = null;
    try {
      StockDetailsSummary stockDetailsByScrip = stocksService
          .getStockDetailsByScrip(scrip.toUpperCase());
      detailsSummaryDTO = mapper.map(stockDetailsByScrip, StockDetailsSummaryDTO.class);
    } catch (KristalAiException ex) {
      return KristalAiHelper
          .getKristalAiHelperResponse(null, List.of(ex.getMessage()), Status.NOT_FOUND);
    } catch (Exception ex) {
      log.error("Something went wrong while getting stock summary. Please try again");
      return KristalAiHelper.getKristalAiHelperResponse(null, List.of(
          "Something went wrong while getting stock summary. Please try again or contact support team"));
    }
    return KristalAiHelper.getKristalAiHelperResponse(detailsSummaryDTO, null);
  }
}
