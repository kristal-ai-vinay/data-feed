package com.kristalai.helpers;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.kristalai.commons.KristalAiResponse;
import com.kristalai.entity.enums.Status;

public class KristalAiHelper {

  public static List<String> getValidationSummary(Set constrains) {
    List<String> errors = new ArrayList<>();
    for (Object constrain : constrains) {
      ConstraintViolation obj = (ConstraintViolation) constrain;
      errors.add(obj.getMessage());
    }
    return errors;
  }

  public static KristalAiResponse getKristalAiHelperResponse(Object data, List<String> errors) {
    if (errors != null && errors.size() > 0) {
      return KristalAiResponse.builder().errors(errors).status(Status.FAILURE)
          .statusCode(Status.FAILURE.getCode()).build();
    } else {
      return KristalAiResponse.builder().data(data).status(Status.SUCCESS)
          .statusCode(Status.SUCCESS.getCode()).build();
    }
  }

  public static KristalAiResponse getKristalAiHelperResponse(Object data, List<String> errors,
      Status status) {
    return KristalAiResponse.builder().data(data).status(status).errors(errors)
        .statusCode(status.getCode()).build();
  }
}
