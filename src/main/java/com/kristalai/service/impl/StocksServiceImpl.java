package com.kristalai.service.impl;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.kristalai.commons.exception.KristalAiException;
import com.kristalai.entity.stocks.StockDetails;
import com.kristalai.entity.stocks.StockDetailsDataHistory;
import com.kristalai.entity.stocks.StockDetailsSummary;
import com.kristalai.repository.StockDetailsRepository;
import com.kristalai.service.StocksService;
import io.github.resilience4j.retry.Retry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

//imports as static

@Service
@AllArgsConstructor
@Slf4j
public class StocksServiceImpl implements StocksService {

  private final StockDetailsRepository stockDetailsRepository;

  private MongoTemplate mongoTemplate;

  @Override
  public void saveStockDetails(StockDetails stockDetails) {
    Retry.ofDefaults("dbOperations")
        .executeRunnable(() -> stockDetailsRepository.save(stockDetails));
  }

  @Override
  public StockDetailsSummary getStockDetailsByScrip(String scrip) {
    boolean exists = stockDetailsRepository.existsByScrip(scrip);
    if (!exists) {
      throw new KristalAiException("No scrip found with name " + scrip);
    }

    float sumOfPricesByScrip = getSumOfPricesByScrip(scrip);
    Instant now = Instant.now();

    List<StockDetailsDataHistory> histories = new ArrayList<>();
    Instant minus5 = now.minus(5, ChronoUnit.MINUTES);
    float fiveMinsSum = getSumOfPricesByScripAndTimeGte(scrip, minus5);
    histories.add(StockDetailsDataHistory.builder().min(5).sum(fiveMinsSum)
        .timeRange(getTimeRangeText(minus5, now)).build());

    Instant minus10 = now.minus(10, ChronoUnit.MINUTES);
    float tenMinsSum = getSumOfPricesByScripAndTimeGte(scrip, minus10);
    histories.add(StockDetailsDataHistory.builder().min(10).sum(tenMinsSum)
        .timeRange(getTimeRangeText(minus10, now)).build());

    Instant minus30 = now.minus(30, ChronoUnit.MINUTES);
    float thirtyMinsSum = getSumOfPricesByScripAndTimeGte(scrip, minus30);
    histories.add(StockDetailsDataHistory.builder().min(30).sum(thirtyMinsSum)
        .timeRange(getTimeRangeText(minus30, now)).build());

    return StockDetailsSummary.builder().scrip(scrip).total(sumOfPricesByScrip).histories(histories)
        .build();
  }

  private float getSumOfPricesByScripAndTimeGte(String scrip, Instant time) {
    Aggregation agg = Aggregation.newAggregation(
        Aggregation.match(new Criteria().and("scrip").in(scrip).and("timeStamp").gt(time)),
        Aggregation.group("scrip").sum("ltp").as("total"));
    AggregationResults<StockDetailsSummary> groupResults = getGroupResults(agg);
    List<StockDetailsSummary> result = groupResults.getMappedResults();
    if (result.size() > 0) {
      return result.get(0).getTotal();
    }
    return 0;
  }

  private float getSumOfPricesByScrip(String scrip) {
    Aggregation agg = Aggregation
        .newAggregation(Aggregation.match(new Criteria().and("scrip").in(scrip)),
            Aggregation.group("scrip").sum("ltp").as("total"));
    AggregationResults<StockDetailsSummary> groupResults = getGroupResults(agg);
    List<StockDetailsSummary> result = groupResults.getMappedResults();
    if (result.size() > 0) {
      return result.get(0).getTotal();
    }
    return 0;
  }

  private AggregationResults<StockDetailsSummary> getGroupResults(Aggregation agg) {
    return mongoTemplate.aggregate(agg, StockDetails.class, StockDetailsSummary.class);
  }

  private String getTimeRangeText(Instant from, Instant to) {
    return from.toString().replaceAll("[TZ]", " ") + "- " + to.toString().replaceAll("[TZ]", " ");
  }
}
