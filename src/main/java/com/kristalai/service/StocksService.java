package com.kristalai.service;

import com.kristalai.entity.stocks.StockDetails;
import com.kristalai.entity.stocks.StockDetailsSummary;

public interface StocksService {

  void saveStockDetails(StockDetails stockDetails);

  StockDetailsSummary getStockDetailsByScrip(String scrip);

}
