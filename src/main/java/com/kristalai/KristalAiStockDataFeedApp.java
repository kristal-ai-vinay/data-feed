package com.kristalai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.schema.client.EnableSchemaRegistryClient;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication(exclude = {
    org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class
})
@EnableBinding(Sink.class)
@EnableMongoAuditing
@EnableSchemaRegistryClient
public class KristalAiStockDataFeedApp {

  public static void main(String[] args) {
    SpringApplication.run(KristalAiStockDataFeedApp.class, args);
  }

}
