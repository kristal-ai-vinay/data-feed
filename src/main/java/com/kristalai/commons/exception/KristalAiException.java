package com.kristalai.commons.exception;

public class KristalAiException extends RuntimeException {

  public KristalAiException(String message) {
    super(message);
  }

  public KristalAiException(String message, Throwable cause) {
    super(message, cause);
  }

  public KristalAiException(Throwable cause) {
    super(cause);
  }

  public KristalAiException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
