package com.kristalai.commons;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kristalai.entity.enums.Status;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KristalAiResponse {

  private Status status;

  private int statusCode;

  private Object data;

  private List<String> errors;

}
