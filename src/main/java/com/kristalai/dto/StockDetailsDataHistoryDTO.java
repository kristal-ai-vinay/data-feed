package com.kristalai.dto;

import lombok.Data;

@Data
public class StockDetailsDataHistoryDTO {

  private int min;

  private float sum;

  private String timeRange;

}
