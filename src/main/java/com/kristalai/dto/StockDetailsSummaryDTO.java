package com.kristalai.dto;

import java.util.List;

import com.kristalai.entity.common.AuditableEntity;
import com.kristalai.entity.stocks.StockDetailsDataHistory;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockDetailsSummaryDTO {

  private String scrip;

  private float total;

  private List<StockDetailsDataHistory> histories;

}
