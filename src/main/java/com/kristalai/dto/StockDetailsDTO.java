package com.kristalai.dto;

import java.time.Instant;

import com.kristalai.entity.common.AuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StockDetailsDTO {

  private String scrip;

  private float ltp;

  private Instant timeStamp;

}
