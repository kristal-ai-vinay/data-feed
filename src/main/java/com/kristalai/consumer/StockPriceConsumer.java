package com.kristalai.consumer;

import com.kristalai.schema.StockDetails;
import com.kristalai.service.StocksService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class StockPriceConsumer {

  private ModelMapper mapper;

  private StocksService stocksService;

  @StreamListener(Sink.INPUT)
  public void consumeStockDetails(StockDetails stockDetails, MessageHeaders messageHeaders) {
    log.info("Let's process stock details: {}", stockDetails);
    try {
      com.kristalai.entity.stocks.StockDetails details = mapper
          .map(stockDetails, com.kristalai.entity.stocks.StockDetails.class);
      stocksService.saveStockDetails(details);
      final Acknowledgment acknowledgment = messageHeaders
          .get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
      if (acknowledgment != null) {
        acknowledgment.acknowledge();
      }
    } catch (Exception ex) {
      log.error("Error while saving stock details ", ex);
    }
  }
}
