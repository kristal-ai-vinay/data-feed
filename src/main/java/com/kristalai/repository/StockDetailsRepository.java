package com.kristalai.repository;

import com.kristalai.entity.stocks.StockDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockDetailsRepository extends MongoRepository<StockDetails, String> {

  boolean existsByScrip(String scrip);
}
